

import * as THREE from './build/three.module.js';
import {PointerLockControls} from './jsm/controls/PointerLockControls.js';
import {ColladaLoader} from './jsm/loaders/ColladaLoader.js';
import {MTLLoader} from './jsm/loaders/MTLLoader.js';
import {OBJLoader} from './jsm/loaders/OBJLoader.js';
import {TGALoader} from './jsm/loaders/TGALoader.js';

var camera, scene, renderer, controls;

var objects = [];
var object;

var raycaster, dir, intersections, pWorld, pLocal, bbox;

var moveForward = false;
var moveBackward = false;
var moveLeft = false;
var moveRight = false;

var prevTime = performance.now();
var velocity = new THREE.Vector3();
var direction = new THREE.Vector3();
var vertex = new THREE.Vector3();
var loader = new OBJLoader();
var textureLoader = new THREE.TextureLoader();
var colladaLoader = new ColladaLoader()

var colorArray = [
  '#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', '#E6B333', '#3366E6',
  '#999966', '#99FF99', '#B34D4D', '#80B300', '#809900', '#E6B3B3', '#6680B3',
  '#66991A', '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC', '#66994D',
  '#B366CC', '#4D8000', '#B33300', '#CC80CC', '#66664D', '#991AFF', '#E666FF',
  '#4DB3FF', '#1AB399', '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
  '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933', '#FF3380', '#CCCC00',
  '#66E64D', '#4D80CC', '#9900B3', '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6',
  '#6666FF'
];

var ceiling, floor;
var ceilingOn = false;
var ceilingColor = ['white'];
var walls = [];
var vendingMachines = [];
var windows = [];
var tables = [];
var booths = [];
var spotlights = [];
var spotlightsOn = [false, false, false, false, false, false, false];

init();
animate();

function init() {
  // Establishes the first person perspective camera
  camera = new THREE.PerspectiveCamera(
      75, window.innerWidth / window.innerHeight, 1, 1000);
  camera.position.y = 10;

  /**********************************************************/

  scene = new THREE.Scene();

  // Distant light source at horizon level
  var light = new THREE.HemisphereLight(0xeeeeff, 0x777788, 0.1);
  light.position.set(0.25, 0, 0.75);
  scene.add(light);

  // Soft white light
  var ambLight = new THREE.AmbientLight(0x404040, 0.5);
  scene.add(ambLight);

  // Series of overhead lights illuminated the cafeteria
  var spotLight1 = new THREE.SpotLight(0xffffff, 0.1);
  scene.add(spotLight1);
  scene.add(spotLight1.target);
  spotlights.push(spotLight1);

  spotLight1.shadow.camera.near = 0.1;
  spotLight1.shadow.camera.far = 1000;
  spotLight1.shadow.mapSize.width = 512;
  spotLight1.shadow.mapSize.height = 512;
  spotLight1.castShadow = true;

  spotLight1.shadow.camera.near = 0.1;
  spotLight1.shadow.camera.far = 1000;
  spotLight1.shadow.camera.fov = 15;

  spotLight1.position.set(0, 25, -70);
  spotLight1.target.position.x = 0;
  spotLight1.target.position.x = 0;
  spotLight1.target.position.z = -70;

  var spotLight2 = spotLight1.clone();
  scene.add(spotLight2);
  scene.add(spotLight2.target);
  spotlights.push(spotLight2);

  spotLight2.position.set(-100, 25, -70);
  spotLight2.target.position.x = -100;
  spotLight2.target.position.x = 0;
  spotLight2.target.position.z = -80;
  spotLight2.castShadow = true;

  var spotLight3 = spotLight1.clone();
  scene.add(spotLight3);
  scene.add(spotLight3.target);
  spotlights.push(spotLight3);

  spotLight3.position.set(-200, 25, -80);
  spotLight3.target.position.x = -200;
  spotLight3.target.position.x = 0;
  spotLight3.target.position.z = -80;
  spotLight3.castShadow = true;

  var spotLight4 = spotLight1.clone();
  scene.add(spotLight4);
  scene.add(spotLight4.target);
  spotlights.push(spotLight4);

  spotLight4.position.set(-300, 25, -80);
  spotLight4.target.position.x = -300;
  spotLight4.target.position.x = 0;
  spotLight4.target.position.z = -80;
  spotLight4.castShadow = true;

  var spotLight5 = spotLight1.clone();
  scene.add(spotLight5);
  scene.add(spotLight5.target);
  spotlights.push(spotLight5);

  spotLight5.position.set(-300, 25, 0);
  spotLight5.target.position.x = -300;
  spotLight5.target.position.x = 0;
  spotLight5.target.position.z = 0;
  spotLight5.castShadow = true;

  var spotLight6 = spotLight1.clone();
  scene.add(spotLight6);
  scene.add(spotLight6.target);
  spotlights.push(spotLight6);

  spotLight6.position.set(-400, 25, -80);
  spotLight6.target.position.x = -400;
  spotLight6.target.position.x = 0;
  spotLight6.target.position.z = -80;
  spotLight6.castShadow = true;

  var spotLight7 = spotLight1.clone();
  scene.add(spotLight7);
  scene.add(spotLight7.target);
  spotlights.push(spotLight7);

  spotLight7.position.set(-400, 25, 0);
  spotLight7.target.position.x = -400;
  spotLight7.target.position.x = 0;
  spotLight7.target.position.z = 0;
  spotLight7.castShadow = true;

  /**********************************************************/

  controls = new PointerLockControls(camera, document.body);

  var blocker = document.getElementById('blocker');
  var instructions = document.getElementById('instructions');

  instructions.addEventListener('click', function() {
    controls.lock();
  }, false);

  controls.addEventListener('lock', function() {
    instructions.style.display = 'none';
    blocker.style.display = 'none';
  });

  controls.addEventListener('unlock', function() {
    blocker.style.display = 'block';
    instructions.style.display = '';
  });

  scene.add(controls.getObject());

  var defaultIntensity = 0.5;
  var colorIdx = 0;

  var onKeyDown = function(event) {
    switch (event.keyCode) {
      case 87:  // w
        moveForward = true;
        break;

      case 65:  // a
        moveLeft = true;
        break;

      case 83:  // s
        moveBackward = true;
        break;

      case 68:  // d
        moveRight = true;
        break;

      case 49:  // 1
        if (spotlightsOn[0]) {
          spotlights[0].intensity = 0;
          spotlightsOn[0] = false;
        } else {
          spotlights[0].intensity = defaultIntensity;
          spotlightsOn[0] = true;
        }
        break;
      case 50:  // 2
        if (spotlightsOn[1]) {
          spotlights[1].intensity = 0;
          spotlightsOn[1] = false;
        } else {
          spotlights[1].intensity = defaultIntensity;
          spotlightsOn[1] = true;
        }
        break;
      case 51:  // 3
        if (spotlightsOn[2]) {
          spotlights[2].intensity = 0;
          spotlightsOn[2] = false;
        } else {
          spotlights[2].intensity = defaultIntensity;
          spotlightsOn[2] = true;
        }
        break;
      case 52:  // 4
        if (spotlightsOn[3]) {
          spotlights[3].intensity = 0;
          spotlightsOn[3] = false;
        } else {
          spotlights[3].intensity = defaultIntensity;
          spotlightsOn[3] = true;
        }
        break;
      case 53:  // 5
        if (spotlightsOn[4]) {
          spotlights[4].intensity = 0;
          spotlightsOn[4] = false;
        } else {
          spotlights[4].intensity = defaultIntensity;
          spotlightsOn[4] = true;
        }
        break;
      case 54:  // 6
        if (spotlightsOn[5]) {
          spotlights[5].intensity = 0;
          spotlightsOn[5] = false;
        } else {
          spotlights[5].intensity = defaultIntensity;
          spotlightsOn[5] = true;
        }
        break;
      case 55:  // 7
        if (spotlightsOn[6]) {
          spotlights[6].intensity = 0;
          spotlightsOn[6] = false;
        } else {
          spotlights[6].intensity = defaultIntensity;
          spotlightsOn[6] = true;
        }
        break;
      case 32:  // 7
        if (ceilingOn) {
          ceiling.material.emissiveIntensity = 0.1;
          light.intensity = 0.1;
          ceilingOn = false;
        } else {
          ceiling.material.emissiveIntensity = 0.7;
          light.intensity = 0.5;
          ceilingOn = true;
        }
        break;
      case 38:  // up
        light.color =
            new THREE.Color(colorArray[(colorIdx++) % (colorArray.length - 1)]);
        break;
      case 40:  // down
        if (colorIdx - 1 >= 0) {
          light.color = new THREE.Color(
              colorArray[(colorIdx--) % (colorArray.length - 1)]);
        }
        break;
      case 39:  // right
        light.color = new THREE.Color('0xFFFFFF');
        break;
      case 37:  // left
        light.color = new THREE.Color('0xFFFFFF');
        break;

      case 79:  // o
        if (light.position.y <= 0) light.position.y += 10;
        break;
      case 80:  // p
        if (light.position.y >= 0) light.position.y -= 10;
        break;
    }
  };

  var onKeyUp = function(event) {
    switch (event.keyCode) {
      case 38:  // up
      case 87:  // w
        moveForward = false;
        break;

      case 37:  // left
      case 65:  // a
        moveLeft = false;
        break;

      case 40:  // down
      case 83:  // s
        moveBackward = false;
        break;

      case 39:  // right
      case 68:  // d
        moveRight = false;
        break;
    }
  };

  document.addEventListener('keydown', onKeyDown, false);
  document.addEventListener('keyup', onKeyUp, false);

  /**********************************************************/

  // Add flooring texture to plane geometry with Lambert

  var floorGeometry = new THREE.PlaneBufferGeometry(2000, 2000, 100, 100);
  floorGeometry.rotateX(-Math.PI / 2);

  var floorTexture = textureLoader.load('textures/flooring.png');
  floorTexture.wrapS = THREE.RepeatWrapping;
  floorTexture.wrapT = THREE.RepeatWrapping;
  floorTexture.repeat.set(120, 120);

  var floorMaterial = new THREE.MeshLambertMaterial({
    map: floorTexture,
    side: THREE.DoubleSide,
    color: 'white',
    reflectivity: 0.5
  });

  var floor = new THREE.Mesh(floorGeometry, floorMaterial);
  floor.receiveShadow = true;

  scene.add(floor);

  /**********************************************************/

  // Add ceiling texture to plane geometry with Lambert

  var ceilingGeometry = new THREE.PlaneBufferGeometry(2000, 2000, 100, 100);

  var ceilingTexture = textureLoader.load('textures/ceiling.png');
  ceilingTexture.wrapS = THREE.RepeatWrapping;
  ceilingTexture.wrapT = THREE.RepeatWrapping;
  ceilingTexture.repeat.set(200, 200);
  ceilingTexture.encoding = THREE.sRGBEncoding;
  ceilingTexture.anisotropy = 16;

  var ceilingMaterial = new THREE.MeshLambertMaterial({
    emissive: 0xFFFFFF,
    emissiveIntensity: 0.1,
    map: ceilingTexture,
    specularMap: textureLoader.load('textures/ceiling_spec.png'),
    side: THREE.DoubleSide
  });

  ceiling = new THREE.Mesh(ceilingGeometry, ceilingMaterial);

  ceilingGeometry.rotateX(-Math.PI / 2);
  scene.add(ceiling);
  ceiling.position.set(100, 30, 0);

  /**********************************************************/

  // Add wall texture to plane geometry with Phong material

  var wallGeometry = new THREE.PlaneBufferGeometry(2000, 2000, 100, 100);

  var wallTexture = textureLoader.load('textures/wall.jpg');
  wallTexture.wrapS = THREE.RepeatWrapping;
  wallTexture.wrapT = THREE.RepeatWrapping;
  wallTexture.repeat.set(100, 100);
  wallTexture.encoding = THREE.sRGBEncoding;
  wallTexture.anisotropy = 16;

  var wallMaterial = new THREE.MeshPhongMaterial({
    map: wallTexture,
    side: THREE.DoubleSide,
  });

  var wall = new THREE.Mesh(wallGeometry, wallMaterial);
  wall.castShadow = true;
  wall.receiveShadow = true;
  wall.position.set(0, 0, -105);
  scene.add(wall);
  objects.push(wall);
  walls.push(wall);

  var instance = wall.clone();
  instance.castShadow = true;
  instance.receiveShadow = true;
  scene.add(instance);
  walls.push(instance);

  objects.push(instance);
  instance.rotateY(-Math.PI / 2);
  instance.position.set(50, 0, 0);

  instance = wall.clone();
  instance.castShadow = true;
  instance.receiveShadow = true;
  scene.add(instance);
  objects.push(instance);
  walls.push(instance);

  instance.rotateY(Math.PI);
  instance.position.set(0, 0, 20);

  instance = wall.clone();
  instance.castShadow = true;
  instance.receiveShadow = true;
  scene.add(instance);
  walls.push(instance);

  objects.push(instance);
  instance.rotateY(-Math.PI / 2);
  instance.position.set(-400, 0, 0);

  wallGeometry = new THREE.BoxGeometry(100, 100, 100);
  var wallTexture = textureLoader.load('textures/wall.jpg');
  wallTexture.wrapS = THREE.RepeatWrapping;
  wallTexture.wrapT = THREE.RepeatWrapping;
  wallTexture.encoding = THREE.sRGBEncoding;
  wallTexture.anisotropy = 16;
  wallTexture.repeat.set(10, 10);

  wallMaterial =
      new THREE.MeshPhongMaterial({map: wallTexture, side: THREE.DoubleSide});
  var cube = new THREE.Mesh(wallGeometry, wallMaterial);
  cube.castShadow = true;
  cube.receiveShadow = true;
  cube.position.set(-70, 0, -5);
  scene.add(cube);
  objects.push(cube);
  walls.push(cube);

  /**********************************************************/

  // Add boxes with window texture and white emissive lighting to emulate
  // windows

  var windowGeometry = new THREE.BoxGeometry(15, 15, 10);
  var windowTexture = textureLoader.load('textures/window.jpg');
  var windowMaterial = new THREE.MeshPhongMaterial({
    map: windowTexture,
    emissive: 0xFFFFFF,
    color: 0xFFFFFF,
    emissiveIntensity: 0.1,
    side: THREE.DoubleSide
  });

  var windowHeight = 17;

  cube = new THREE.Mesh(windowGeometry, windowMaterial);
  cube.position.set(-20, windowHeight, -109.5);
  scene.add(cube);
  objects.push(cube);
  windows.push(cube);

  instance = cube.clone();
  scene.add(instance);
  windows.push(instance);
  objects.push(instance);
  cube.position.set(-50, windowHeight, -109.5);

  instance = cube.clone();
  scene.add(instance);
  windows.push(instance);
  objects.push(instance);
  cube.position.set(-80, windowHeight, -109.5);

  instance = cube.clone();
  scene.add(instance);
  windows.push(instance);
  cube.position.set(-110, windowHeight, -109.5);

  instance = cube.clone();
  scene.add(instance);
  windows.push(instance);
  objects.push(instance);
  cube.position.set(-140, windowHeight, -109.5);

  instance = cube.clone();
  scene.add(instance);
  windows.push(instance);
  objects.push(instance);
  cube.position.set(-170, windowHeight, -109.5);

  instance = cube.clone();
  scene.add(instance);
  windows.push(instance);
  objects.push(instance);
  cube.position.set(-200, windowHeight, -109.5);

  instance = cube.clone();
  scene.add(instance);
  windows.push(instance);
  objects.push(instance);
  cube.position.set(-230, windowHeight, -109.5);

  instance = cube.clone();
  scene.add(instance);
  windows.push(instance);
  objects.push(instance);
  cube.position.set(-260, windowHeight, -109.5);

  instance = cube.clone();
  scene.add(instance);
  windows.push(instance);
  objects.push(instance);
  cube.position.set(-290, windowHeight, -109.5);

  instance = cube.clone();
  scene.add(instance);
  windows.push(instance);
  objects.push(instance);
  cube.position.set(-320, windowHeight, -109.5);

  instance = cube.clone();
  scene.add(instance);
  windows.push(instance);
  objects.push(instance);
  cube.position.set(-350, windowHeight, -109.5);

  instance = cube.clone();
  scene.add(instance);
  windows.push(instance);
  objects.push(instance);
  cube.position.set(-380, windowHeight, -109.5);

  /**********************************************************/

  // Add wood texture to table obj model sourced from
  // https://resources.blogscopia.com/2010/04/22/table-and-chairs-download/

  var woodTexture =
      textureLoader.load('textures/booth-restaurant/model/Wood-Oak.jpg');
  woodTexture.wrapS = THREE.RepeatWrapping;
  woodTexture.wrapT = THREE.RepeatWrapping;
  woodTexture.encoding = THREE.sRGBEncoding;
  woodTexture.anisotropy = 16;
  woodTexture.repeat.set(1, 1);

  var woodMaterial = new THREE.MeshPhongMaterial({
    map: woodTexture,
  });

  var onProgress = function(xhr) {
    if (xhr.lengthComputable) {
      var percentComplete = xhr.loaded / xhr.total * 100;
      console.log(Math.round(percentComplete, 2) + '% downloaded');
    }
  };

  var onError = function() {};

  var manager = new THREE.LoadingManager();

  new MTLLoader(manager).load(
      'models/table-chairs/table_and_chairs.mtl', function(materials) {
        materials.preload();
        new OBJLoader(manager).load(
            'models/table-chairs/table_and_chairs.obj', function(object) {
              object.traverse(function(child) {
                if (child instanceof THREE.Mesh) {
                  child.material = woodMaterial;
                }
              });
              object.position.z = -50;
              object.position.x = 30;
              object.scale.set(12, 12, 12);
              object.castShadow = true;
              object.receiveShadow = true;
              scene.add(object);
              tables.push(object);
              objects.push(object);

              instance = object.clone();
              instance.position.x = 30;
              instance.position.z = -25;
              instance.scale.set(12, 12, 12);
              instance.castShadow = true;
              instance.receiveShadow = true;
              instance.traverse(function(child) {
                child.castShadow = true;
              })
              scene.add(instance);
              tables.push(instance);
              objects.push(instance);

              instance = object.clone();
              instance.position.x = 30;
              instance.position.z = -75;
              instance.scale.set(12, 12, 12);
              instance.castShadow = true;
              instance.receiveShadow = true;
              instance.traverse(function(child) {
                child.castShadow = true;
              })
              scene.add(instance);
              tables.push(instance);
              objects.push(instance);

              instance = object.clone();
              instance.position.x = -20;
              instance.position.z = -95;
              instance.rotateY(-Math.PI / 2);
              instance.scale.set(12, 12, 12);
              instance.castShadow = true;
              instance.receiveShadow = true;
              instance.traverse(function(child) {
                child.castShadow = true;
              })
              scene.add(instance);
              tables.push(instance);
              objects.push(instance);

              instance = object.clone();
              instance.position.x = -40;
              instance.position.z = -95;
              instance.rotateY(-Math.PI / 2);
              instance.scale.set(12, 12, 12);
              instance.castShadow = true;
              instance.receiveShadow = true;
              instance.traverse(function(child) {
                child.castShadow = true;
              })
              scene.add(instance);
              tables.push(instance);
              objects.push(instance);

              instance = object.clone();
              instance.position.x = -60;
              instance.position.z = -95;
              instance.rotateY(-Math.PI / 2);
              instance.scale.set(12, 12, 12);
              instance.castShadow = true;
              instance.receiveShadow = true;
              instance.traverse(function(child) {
                child.castShadow = true;
              })
              scene.add(instance);
              tables.push(instance);
              objects.push(instance);

              instance = object.clone();
              instance.position.x = -80;
              instance.position.z = -95;
              instance.rotateY(-Math.PI / 2);
              instance.scale.set(12, 12, 12);
              instance.castShadow = true;
              instance.receiveShadow = true;
              instance.traverse(function(child) {
                child.castShadow = true;
              })
              scene.add(instance);
              tables.push(instance);
              objects.push(instance);

              instance = object.clone();
              instance.position.x = -100;
              instance.position.z = -95;
              instance.rotateY(-Math.PI / 2);
              instance.scale.set(12, 12, 12);
              instance.castShadow = true;
              instance.receiveShadow = true;
              instance.traverse(function(child) {
                child.castShadow = true;
              })
              scene.add(instance);
              tables.push(instance);
              objects.push(instance);

              instance = object.clone();
              instance.position.x = -170;
              instance.position.z = -50;
              instance.rotateY(-Math.PI / 2);
              instance.scale.set(12, 12, 12);
              instance.castShadow = true;
              instance.receiveShadow = true;
              instance.traverse(function(child) {
                child.castShadow = true;
              })
              scene.add(instance);
              tables.push(instance);
              objects.push(instance);

              instance = object.clone();
              instance.position.x = -190;
              instance.position.z = -50;
              instance.rotateY(-Math.PI / 2);
              instance.scale.set(12, 12, 12);
              instance.castShadow = true;
              instance.receiveShadow = true;
              instance.traverse(function(child) {
                child.castShadow = true;
              })
              scene.add(instance);
              tables.push(instance);
              objects.push(instance);

              instance = object.clone();
              instance.position.x = -210;
              instance.position.z = -50;
              instance.rotateY(-Math.PI / 2);
              instance.scale.set(12, 12, 12);
              instance.castShadow = true;
              instance.receiveShadow = true;
              instance.traverse(function(child) {
                child.castShadow = true;
              })
              scene.add(instance);
              tables.push(instance);
              objects.push(instance);

              instance = object.clone();
              instance.position.x = -230;
              instance.position.z = -50;
              instance.rotateY(-Math.PI / 2);
              instance.scale.set(12, 12, 12);
              instance.castShadow = true;
              instance.receiveShadow = true;
              instance.traverse(function(child) {
                child.castShadow = true;
              })
              scene.add(instance);
              tables.push(instance);
              objects.push(instance);

              instance = object.clone();
              instance.position.x = -250;
              instance.position.z = -50;
              instance.rotateY(-Math.PI / 2);
              instance.scale.set(12, 12, 12);
              instance.castShadow = true;
              instance.receiveShadow = true;
              instance.traverse(function(child) {
                child.castShadow = true;
              })
              scene.add(instance);
              tables.push(instance);
              objects.push(instance);

              instance = object.clone();
              instance.position.x = -270;
              instance.position.z = -50;
              instance.rotateY(-Math.PI / 2);
              instance.scale.set(12, 12, 12);
              instance.castShadow = true;
              instance.receiveShadow = true;
              instance.traverse(function(child) {
                child.castShadow = true;
              })
              scene.add(instance);
              tables.push(instance);
              objects.push(instance);
            }, onProgress, onError);
      });

  /**********************************************************/

  // Coke machine model and maps sourced from
  // https://www.turbosquid.com/3d-models/obj-mode-element/705261
  loader.load(
      // resource URL
      'models/cokemachine/Mesh/CokeMachine.obj',
      // called when resource is loaded
      function(object) {
        object.traverse(function(child) {
          if (child instanceof THREE.Mesh) {
            child.material = new THREE.MeshLambertMaterial({
              emissive: 0xff6961,
              emissiveIntensity: 0.15,
              map: textureLoader.load(
                  'textures/cokemachine/Maps/DiffuseMap.jpg'),
              specularMap:
                  textureLoader.load('textures/cokemachine/Maps/SpecMap.jpg'),
              side: THREE.DoubleSide
            });
            child.castShadow = true;
          }
        });
        object.scale.set(0.2, 0.2, 0.2);
        object.position.set(-5, 0, -100);
        object.castShadow = true;
        object.receiveShadow = true;
        vendingMachines.push(object);
        scene.add(object);
        objects.push(object);

        var instance = object.clone();
        instance.scale.set(0.2, 0.2, 0.2);
        instance.position.set(5, 0, -100);
        instance.castShadow = true;
        instance.receiveShadow = true;
        instance.traverse(function(child) {
          child.castShadow = true;
        })
        scene.add(instance);
        vendingMachines.push(instance);
        objects.push(instance);

        instance = object.clone();
        instance.scale.set(0.2, 0.2, 0.2);
        instance.position.set(15, 0, -100);
        instance.castShadow = true;
        instance.receiveShadow = true;
        instance.traverse(function(child) {
          child.castShadow = true;
        })
        scene.add(instance);
        vendingMachines.push(instance);
        objects.push(instance);

        instance = object.clone();
        instance.scale.set(0.2, 0.2, 0.2);
        instance.position.set(-300, 0, 16);
        instance.castShadow = true;
        instance.receiveShadow = true;
        instance.rotateY(Math.PI);
        instance.traverse(function(child) {
          child.castShadow = true;
        })
        scene.add(instance);
        vendingMachines.push(instance);
        objects.push(instance);

        instance = object.clone();
        instance.scale.set(0.2, 0.2, 0.2);
        instance.position.set(-290, 0, 16);
        instance.castShadow = true;
        instance.receiveShadow = true;
        instance.rotateY(Math.PI);
        instance.traverse(function(child) {
          child.castShadow = true;
        })
        scene.add(instance);
        vendingMachines.push(instance);
        objects.push(instance);
      },
  );

  /**********************************************************/

  // Restaurant booth model sourced from
  // https://3dwarehouse.sketchup.com/model/u94e5e652-a67e-48b6-81a6-e05df3dfb8ee/Plymold-Contour-Laminate-Booth-Seating

  var boothTexture =
      textureLoader.load('models/booth-restaurant/model/Wood-Oak.jpg');
  boothTexture.wrapS = THREE.RepeatWrapping;
  boothTexture.wrapT = THREE.RepeatWrapping;
  boothTexture.repeat.set(2, 2);
  boothTexture.encoding = THREE.sRGBEncoding;
  boothTexture.anisotropy = 16;
  var boothMaterial = new THREE.MeshPhongMaterial({
    map: boothTexture,
    specular: 'brown',
    shininess: 0.5,
    side: THREE.DoubleSide
  });

  loader.load('models/booth-restaurant/model.obj', function(object) {
    object.traverse(function(node) {
      if (node.isMesh) node.material = boothMaterial;
    });

    object.scale.set(0.3, 0.3, 0.3);
    object.traverse(function(child) {
      child.castShadow = true;
      object.receiveShadow = true;
    });
    object.position.set(-140, 0, -90);
    scene.add(object);
    booths.push(object);
    objects.push(object);


    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.position.set(-160, 0, -90);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.position.set(-180, 0, -90);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.position.set(-200, 0, -90);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.position.set(-220, 0, -90);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.position.set(-240, 0, -90);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.position.set(-260, 0, -90);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.position.set(-280, 0, -90);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.position.set(-300, 0, -90);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.position.set(-320, 0, -90);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.position.set(-340, 0, -90);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.position.set(-360, 0, -90);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.position.set(-380, 0, -90);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.rotateY((Math.PI / 2));
    instance.position.set(-385, 0, -65);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.castShadow = true;
    instance.receiveShadow = true;
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.rotateY((Math.PI / 2));
    instance.position.set(-385, 0, -44);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.rotateY((Math.PI / 2));
    instance.position.set(-385, 0, -23);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.rotateY((Math.PI / 2));
    instance.position.set(-385, 0, -2);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.rotateY(Math.PI);
    instance.position.set(-360, 0, 4);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.rotateY(Math.PI);
    instance.position.set(-250, 0, 4);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.rotateY(Math.PI);
    instance.position.set(-225, 0, 4);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.rotateY(Math.PI);
    instance.position.set(-200, 0, 4);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.rotateY(Math.PI);
    instance.position.set(-175, 0, 4);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);

    instance = object.clone();
    instance.scale.set(0.3, 0.3, 0.3);
    instance.traverse(function(child) {
      child.castShadow = true;
      instance.receiveShadow = true;
    });
    instance.rotateY(Math.PI);
    instance.position.set(-150, 0, 4);
    scene.add(instance);
    booths.push(instance);
    objects.push(instance);
  });

  /**********************************************************/

  // Double Door booth model sourced from
  // https://www.turbosquid.com/3d-models/ready-pbr---3d-model-1316050

  var tgaLoader = new TGALoader();

  var doorTexture = tgaLoader.load('models/double-door/TX_Door_01_ALB.tga');
  var doorMaterial = new THREE.MeshPhongMaterial(
      {color: 0xffffff, map: doorTexture, side: THREE.DoubleSide});

  loader.load('models/double-door/SM_Door_01.obj', function(object) {
    object.scale.set(0.085, 0.085, 0.085);
    object.traverse(function(child) {
      child.material = doorMaterial;
      child.castShadow = true;
      child.receiveShadow = true;
    });
    object.position.set(15, 0, 20);
    object.rotateY(-Math.PI);
    scene.add(object);
    objects.push(object);

    instance = object.clone();
    instance.position.set(-20, 0, -15);
    instance.rotateY(-Math.PI / 2);
    scene.add(instance);
    objects.push(instance);

    instance = object.clone();
    instance.position.set(-120, 0, -15);
    instance.rotateY(Math.PI / 2);
    scene.add(instance);
    objects.push(instance);

    instance = object.clone();
    instance.position.set(-330, 0, 20);
    scene.add(instance);
    objects.push(instance);
  });

  /**********************************************************/

  // Column defined from built in cylinder geometry and wall texture

  var columnGeometry = new THREE.CylinderGeometry(6, 6, 40, 15);
  var columnMaterial =
      new THREE.MeshPhongMaterial({map: wallTexture, shininess: 0.5});
  var cylinder = new THREE.Mesh(columnGeometry, columnMaterial);
  cylinder.position.y = 10;
  cylinder.position.x = -320;
  cylinder.position.z = -40;
  cylinder.receiveShadow = true;
  scene.add(cylinder);
  objects.push(cylinder);

  /**********************************************************/

  renderer = new THREE.WebGLRenderer({antialias: true});
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  //

  window.addEventListener('resize', onWindowResize, false);
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
  requestAnimationFrame(animate);

  if (controls.isLocked === true) {
    // The camera perspective is used to define the raycaster vector in
    // reference to the scene
    var vector = new THREE.Vector3(0, 0, -1);
    vector = camera.localToWorld(vector);
    vector.sub(camera.position);

    raycaster = new THREE.Raycaster(camera.position, vector);
    raycaster.ray.origin.copy(controls.getObject().position);

    // Checks intersection of raycaster and object array
    intersections = raycaster.intersectObjects(objects, true);

    var onObject = intersections.length > 0;

    var time = performance.now();
    var delta = (time - prevTime) / 1000;

    velocity.x -= velocity.x * 10.0 * delta;
    velocity.z -= velocity.z * 10.0 * delta;

    direction.z = Number(moveForward) - Number(moveBackward);
    direction.x = Number(moveRight) - Number(moveLeft);
    direction.normalize();

    if (moveForward || moveBackward) velocity.z -= direction.z * 400.0 * delta;
    if (moveLeft || moveRight) velocity.x -= direction.x * 400.0 * delta;

    // If intersections
    if (onObject === true) {
      for (var i = 0; i < intersections.length; i++) {
        // Detect the collision between the object and camera
        if (intersections[i].distance < 10) {
          // Stop movement if collided
          velocity.x = Math.max(0, velocity.x);
          velocity.z = Math.max(0, velocity.z);
        };
      }
    }

    controls.moveRight(-velocity.x * delta);
    controls.moveForward(-velocity.z * delta);

    prevTime = time;
  }
  renderer.render(scene, camera);
}